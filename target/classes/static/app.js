var stompClient = null;

function setConnected(connected) {
	$("#connect").prop("disabled", connected);
	$("#disconnect").prop("disabled", !connected);
	if (connected) {
		$("#conversation").show();
	} else {
		$("#conversation").hide();
	}
	$("#traces").html("");
	$("#traceSpanStatistic").text("");
}

function connect() {
	var socket = new SockJS('/trace-websocket');
	stompClient = Stomp.over(socket);
	stompClient.connect({}, function(frame) {
		setConnected(true);
		console.log('Connected: ' + frame);
		stompClient.subscribe('/topic/traces', function(trace) {
			showTrace(trace.body);
		});
		stompClient.subscribe('/topic/statistics', function(map) {
			showStatistic(map.body);
		});
	});
}

function disconnect() {
	if (stompClient !== null) {
		stompClient.disconnect();
	}
	setConnected(false);
	console.log("Disconnected");
}

function showStatistic(map) {
	$("#traceSpanStatistic").text(map);
}

var eventCount = 0;
function showTrace(message) {
	var rowCount = $("#traces > tr").children().length;
	eventCount++;
	$("#traces").prepend(
			"<tr><td>" + eventCount + "</td><td><pre>" + message
					+ "</pre></td></tr>");
	if (rowCount > 18)
		$("#traces").find("tr:last").remove();
}

$(function() {
	$("form").on('submit', function(e) {
		e.preventDefault();
	});
	$("#connect").click(function() {
		connect();
	});
	$("#disconnect").click(function() {
		disconnect();
	});
	$("#send").click(function() {
		sendName();
	});
});
