package de.cf.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.cf.model.AttributeValue;


public interface AttributeValueRepository extends MongoRepository<AttributeValue, String> {
    Optional<AttributeValue> findByPageIdAndAndName(String pageId, String name);
    List<AttributeValue> findByPageId(String pageId);

}
