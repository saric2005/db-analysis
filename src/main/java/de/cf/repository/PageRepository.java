package de.cf.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import de.cf.model.Page;

public interface PageRepository extends MongoRepository<Page, String> {

}
