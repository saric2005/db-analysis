package de.cf.model;


import static de.cf.model.TypeDefinition.COLLECTION_NAME;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.google.common.collect.Lists;

import lombok.Data;

@Data
@Document(collection = COLLECTION_NAME)
public class TypeDefinition {
    public static final String COLLECTION_NAME = "typedefinition";
    @Id private String id;
    private String name;
    @Valid List<AttributeDefinition> attributeDefinitions = Lists.newArrayList();


}
