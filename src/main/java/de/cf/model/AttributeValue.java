package de.cf.model;

import org.springframework.data.annotation.Transient;

import lombok.Data;

@Data
public class AttributeValue<T> {
    private String id;
    private String name;
    private String pageId;

    // both part of the AttributeDefinition
    private String type;

    @Transient private Class<T> javaType;

    private T value;

    public AttributeValue() {
    }


    @Override
    public String toString() {
        return "AttributeValue{" +
                "id=" + id +
                ", name=" + name +
                ", type=" + type +
                ", javaType=" + javaType +
                ", value=" + value +
                '}';
    }
}
