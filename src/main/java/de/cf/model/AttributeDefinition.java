package de.cf.model;

import lombok.Data;

@Data
public class AttributeDefinition {

    private String id;
    private String name;
    private String type;
    private String multiplicity;

}
