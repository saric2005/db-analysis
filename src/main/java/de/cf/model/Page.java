package de.cf.model;

import static de.cf.model.Page.COLLECTION_NAME;

import javax.validation.Valid;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Builder;
import lombok.Data;

@Data
@Document(collection = COLLECTION_NAME)
@Builder
public class Page {
    public static final String COLLECTION_NAME = "page";
    @Valid
    private String typeDefinitionId;
    @Id
    private String id;
    private String name;
}
