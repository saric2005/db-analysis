package de.cf.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.cf.model.AttributeValue;
import de.cf.model.Page;
import de.cf.service.AttributeService;
import de.cf.service.PageService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/page")
public class PageController {

    @Autowired
    PageService pageService;

    @Autowired
    AttributeService attributeService;

    @GetMapping
    public List<Page> getPageAllPages() {
        return pageService.getPageAllPages();
    }

    @GetMapping("/{pageId}")
    public Optional<Page> getPageById(@PathVariable String pageId) {
        return pageService.getPageById(pageId);
    }

    @GetMapping("/{pageId}/attribute")
    public List<AttributeValue> getAllAttributeValue(@PathVariable String pageId) {
        return attributeService.getAllAttributeValue(pageId);
    }

    @GetMapping("/{pageId}/attribute/{name}")
    public Optional<AttributeValue> getAttributeByNameAndPage(@PathVariable String pageId, @PathVariable String name) {
        return attributeService.getAttributeByNameAndPage(pageId, name);
    }
}
