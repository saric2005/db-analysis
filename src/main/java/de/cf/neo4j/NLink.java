/*
package de.cf.neo4j;

import org.neo4j.ogm.annotation.*;

@RelationshipEntity(type = "LINK")
public class NLink {

    @GraphId
    private Long id;

    @Index(primary = true, unique = true)
    private String dbId;

    @Property
    private String type;

    @StartNode
    private NPage source;

    @EndNode
    private NPage target;

    public Long getId() {
        return id;
    }

    public NLink setId(Long id) {
        this.id = id;
        return this;
    }

    public String getDbId() {
        return dbId;
    }

    public NLink setDbId(String dbId) {
        this.dbId = dbId;
        return this;
    }

    public String getType() {
        return type;
    }

    public NLink setType(String type) {
        this.type = type;
        return this;
    }

    public NPage getSource() {
        return source;
    }

    public NLink setSource(NPage source) {
        this.source = source;
        return this;
    }

    public NPage getTarget() {
        return target;
    }

    public NLink setTarget(NPage target) {
        this.target = target;
        return this;
    }
}
*/
