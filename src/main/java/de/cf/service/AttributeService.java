package de.cf.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.cf.model.AttributeValue;
import de.cf.repository.AttributeValueRepository;

@Service
public class AttributeService {

    @Autowired
    AttributeValueRepository attributeValueRepository;

    public List<AttributeValue> getAllAttributeValue(String pageId) {
        return attributeValueRepository.findByPageId(pageId);
    }

    public Optional<AttributeValue> getAttributeByNameAndPage(String pageId, String name) {
        return attributeValueRepository.findByPageIdAndAndName(pageId, name);
    }
}
