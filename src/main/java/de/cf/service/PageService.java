package de.cf.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.cf.model.Page;
import de.cf.repository.PageRepository;

@Service
public class PageService {
    @Autowired
    PageRepository pageRepository;

    public Optional<Page> getPageById(String pageId) {
        return pageRepository.findById(pageId);
    }

    public List<Page> getPageAllPages() {
        return pageRepository.findAll();
    }
}
