package de.cf.db;

public class Page {

    private String id;
    private String uid;
    private String tenant;
    private String space;
    private String name;
    private String typeDef;
    private String attributes;

    public String getId() {
        return id;
    }

    public String getUid() {
        return uid;
    }

    public Page setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Page setId(String id) {
        this.id = id;
        return this;
    }

    public String getTenant() {
        return tenant;
    }

    public Page setTenant(String tenant) {
        this.tenant = tenant;
        return this;
    }

    public String getSpace() {
        return space;
    }

    public Page setSpace(String space) {
        this.space = space;
        return this;
    }

    public String getName() {
        return name;
    }

    public Page setName(String name) {
        this.name = name;
        return this;
    }

    public String getTypeDef() {
        return typeDef;
    }

    public Page setTypeDef(String typeDef) {
        this.typeDef = typeDef;
        return this;
    }

    public String getAttributes() {
        return attributes;
    }

    public Page setAttributes(String attributes) {
        this.attributes = attributes;
        return this;
    }


    @Override
    public String toString() {
        return "Page{" +
//                "id=" + id +
                "uid=" + uid +
                ", tenant=" + tenant +
                ", space=" + space +
                ", name=" + name +
                ", typeDef=" + typeDef +
//                ", attributes=" + attributes +
                '}';
    }
}
