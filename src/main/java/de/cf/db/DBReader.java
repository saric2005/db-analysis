package de.cf.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * [{"name":"activityClass","values":[{"link":"page/6itxefq85u2aw1xkh97izgols"}]},{"name":"alias","values":[{"string":"Best. Karo. Dyn."}]},{"name":"automaticScheduling","values":[{"string":"true"}]},{"name":"calendarId","values":[{"string":"standard"}]},{"name":"cf.cplace.projektplanung.pptexport.showMilestoneTimeInterval","values":[{"string":"false"}]},{"name":"containingSchedule","values":[{"link":"page/l1903sb2m06wtsxvi0tjdm5w2"}]},{"name":"date","values":[{"dateValue":"1551394800000"}]},{"name":"dateCalendarWeek","values":[{"dateValue":"1551049200000"}]},{"name":"hideOnSubtaskRollup","values":[{"string":"true"}]},{"name":"parent","values":[{"link":"page/abo6bm1kur55x4skwy2czt3nj"}]},{"name":"percentComplete","values":[{"numberValue":"0.0"}]},{"name":"protectSubscriptions","values":[{"string":"true"}]},{"name":"rollup","values":[{"string":"false"}]},{"name":"rplan.originalDate.actualTaskDate","values":[{"string":"Milestone at 28.06.2016 (Morning)"}]},{"name":"rplan.originalDate.actualTaskDate.differsFromMappedDate","values":[{"boolean":true}]},{"name":"rplan.originalDate.targetTaskDate.differsFromMappedDate","values":[{"boolean":false}]},{"name":"rplan.rplanId","values":[{"string":"RPLAN-27312561"}]}]
 */
@Repository
public class DBReader {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Link> readLinks() {
        return jdbcTemplate.query(
                "select id, tenantid, usource, utarget, usourceproperty from link_",
                new LinkMapper());
    }

    public List<Page> readPages() {
        return jdbcTemplate.query(
                "select id, u_uid, uspace, tenantid,  uhybridtype, uhybridproperties, uname from path_",
                new PageMapper());

    }

    public List<TypeDefinition> readTypeDefinitions() {
        return jdbcTemplate.query(
                " select id,tenantid, uspace, uname from typedefinition_ order by uname",
                new TypeDefinitionMapper());

    }

}
