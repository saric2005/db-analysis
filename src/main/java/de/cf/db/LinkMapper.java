package de.cf.db;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * select id, tenantid, usource, utarget, usourceproperty from link_;
 */
public class LinkMapper implements RowMapper<Link> {

    @Override
    public Link mapRow(ResultSet rs, int i) throws SQLException {
        return new Link()
                .setId(rs.getString("id"))
                .setTenant(rs.getString("tenantid"))
                .setSource(rs.getString("usource"))
                .setTarget(rs.getString("utarget"))
                .setType(rs.getString("usourceproperty"));
    }
}
