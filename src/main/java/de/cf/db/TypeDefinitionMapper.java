package de.cf.db;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * select id,tenantid, uspace, uname from typedefinition_ order by uname;
 */
public class TypeDefinitionMapper implements RowMapper<TypeDefinition> {

    @Override
    public TypeDefinition mapRow(ResultSet rs, int i) throws SQLException {
        return new TypeDefinition()
                .setId(rs.getString("id"))
                .setSpace(rs.getString("uspace"))
                .setTenant(rs.getString("tenantid"))
                .setName("uname");
    }
}
