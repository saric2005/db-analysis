package de.cf.db;

public class Link {

    private String id;
    private String tenant;
    private String source;
    private String target;
    private String type;

    public String getId() {
        return id;
    }

    public Link setId(String id) {
        this.id = id;
        return this;
    }

    public String getTenant() {
        return tenant;
    }

    public Link setTenant(String tenant) {
        this.tenant = tenant;
        return this;
    }

    public String getSource() {
        return source;
    }

    public Link setSource(String source) {
        this.source = source;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public Link setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getType() {
        return type;
    }

    public Link setType(String type) {
        this.type = type;
        return this;
    }


    @Override
    public String toString() {
        return "Link{" +
                "id=" + id +
                ", tenant=" + tenant +
                ", source=" + source +
                ", target=" + target +
                ", type=" + type +
                '}';
    }
}
