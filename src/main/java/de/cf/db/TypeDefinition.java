package de.cf.db;

public class TypeDefinition {

    private String id;
    private String tenant;
    private String space;
    private String name;


    public String getId() {
        return id;
    }

    public TypeDefinition setId(String id) {
        this.id = id;
        return this;
    }

    public String getTenant() {
        return tenant;
    }

    public TypeDefinition setTenant(String tenant) {
        this.tenant = tenant;
        return this;
    }

    public String getName() {
        return name;
    }

    public TypeDefinition setName(String name) {
        this.name = name;
        return this;
    }

    public String getSpace() {
        return space;
    }

    public TypeDefinition setSpace(String space) {
        this.space = space;
        return this;
    }


    @Override
    public String toString() {
        return "TypeDefinition{" +
                "id=" + id +
                ", tenant=" + tenant +
                ", space=" + space +
                ", name=" + name +
                '}';
    }
}
