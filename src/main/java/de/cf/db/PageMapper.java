package de.cf.db;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PageMapper implements RowMapper<Page> {

    @Override
    public Page mapRow(ResultSet rs, int i) throws SQLException {
       return new Page()
               .setId(rs.getString("id"))
               .setUid(rs.getString("u_uid"))
               .setSpace(rs.getString("uspace"))
               .setTenant(rs.getString("tenantid"))
               .setName(rs.getString("uname"))
               .setTypeDef(rs.getString("uhybridtype"))
               .setAttributes(rs.getString("uhybridproperties"));
    }
}
