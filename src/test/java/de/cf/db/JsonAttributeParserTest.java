package de.cf.db;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import de.cf.model.AttributeValue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonAttributeParserTest {

    @Autowired
    ObjectMapper objectMapper;

    private static String JSON = "[{\"name\":\"activityClass\",\"values\":[{\"link\":\"page/6itxefq85u2aw1xkh97izgols\"}]},{\"name\":\"alias\",\"values\":[{\"string\":\"Best. Karo. Dyn.\"}]},{\"name\":\"automaticScheduling\",\"values\":[{\"string\":\"true\"}]},{\"name\":\"calendarId\",\"values\":[{\"string\":\"standard\"}]},{\"name\":\"cf.cplace.projektplanung.pptexport.showMilestoneTimeInterval\",\"values\":[{\"string\":\"false\"}]},{\"name\":\"containingSchedule\",\"values\":[{\"link\":\"page/l1903sb2m06wtsxvi0tjdm5w2\"}]},{\"name\":\"date\",\"values\":[{\"dateValue\":\"1551394800000\"}]},{\"name\":\"dateCalendarWeek\",\"values\":[{\"dateValue\":\"1551049200000\"}]},{\"name\":\"hideOnSubtaskRollup\",\"values\":[{\"string\":\"true\"}]},{\"name\":\"parent\",\"values\":[{\"link\":\"page/abo6bm1kur55x4skwy2czt3nj\"}]},{\"name\":\"percentComplete\",\"values\":[{\"numberValue\":\"0.0\"}]},{\"name\":\"protectSubscriptions\",\"values\":[{\"string\":\"true\"}]},{\"name\":\"rollup\",\"values\":[{\"string\":\"false\"}]},{\"name\":\"rplan.originalDate.actualTaskDate\",\"values\":[{\"string\":\"Milestone at 28.06.2016 (Morning)\"}]},{\"name\":\"rplan.originalDate.actualTaskDate.differsFromMappedDate\",\"values\":[{\"boolean\":true}]},{\"name\":\"rplan.originalDate.targetTaskDate.differsFromMappedDate\",\"values\":[{\"boolean\":false}]},{\"name\":\"rplan.rplanId\",\"values\":[{\"string\":\"RPLAN-27312561\"}]}]";

    @Test
    public void test() throws IOException {
        final List<AttributeValue> att = getAtt(JSON);
    }


    public static List<AttributeValue> getAtt(String json) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<Mapping> results = mapper.readValue(json.getBytes("UTF-8"), new TypeReference<List<Mapping>>() {
        });

        return results.stream().map(m -> {
            System.out.println(m);
            Class<?> type = getJavaType(m.getType());
            return get(m, type);
        }).collect(Collectors.toList());

    }


    static <T> AttributeValue<T> get(Mapping m, Class<T> type) {
        AttributeValue<T> attributeDefinition = new AttributeValue<>();
        attributeDefinition.setName(m.getName());
        attributeDefinition.setType(m.getType());
        attributeDefinition.setValue(type.cast(getCastValue(m.getType(), m.getValue())));
        attributeDefinition.setJavaType(type);
        return attributeDefinition;
    }

    private static Class<?> getJavaType(String type) {
        switch (type) {
            case "link":
                return String.class;
            case "dateValue":
                return Date.class;
            case "numberValue":
                return Double.class;
            case "boolean":
                return Boolean.class;
            case "string":
            case "localizedString":
            case "richStringValue":
                return String.class;
            case "constrainedValue":
                return String.class;
            default:
                throw new IllegalArgumentException(String.format("Unknown value type! type=%s, Value=%s", type));
        }
    }

    private static Object getCastValue(String type, String value) {
        switch (type) {
            case "link":
                return value;
            case "dateValue":
                return new Date(Long.valueOf(value));
            case "numberValue":
                return Double.parseDouble(value);
            case "boolean":
                return Boolean.parseBoolean(value);
            case "string":
            case "localizedString":
            case "richStringValue":
            case "constrainedValue":
                return value;
            default:
                throw new IllegalArgumentException(String.format("Unknown value type! type=%s, Value=%s", type, value));
        }
    }

    static class Mapping {
        private String name;
        private Map<String, String>[] values;

        private String type;
        private String value;


        public String getName() {
            return name;
        }

        public Mapping setName(String name) {
            this.name = name;
            return this;
        }

        public Map<String, String>[] getValues() {
            return values;
        }

        public Mapping setValues(Map<String, String>[] values) {
            this.values = values;
            values[0].forEach((k, v) -> {
                type = k;
                value = v;
            });
            return this;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "Mapping{" +
                    "name=" + name +
                    ", values=" + Arrays.toString(values) +
                    '}';
        }
    }
}