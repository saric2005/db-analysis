package de.cf.db;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import de.cf.model.AttributeValue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DBReaderTest {

    @Autowired
    private DBReader dbReader;

    @Test
    public void testReadLinks() {
        List<Link> links = dbReader.readLinks();
//                        .forEach(System.err::println);

        System.err.println(links.size());
    }

    @Test
    public void testReadPages() {
        final List<Page> pages = dbReader.readPages();
        final List<de.cf.model.Page> pageList = pages.stream().map(this::getPages).collect(Collectors.toList());
        pages.stream().map(this::getAttributes).collect(Collectors.toList());
        pages
                .forEach(System.err::println);
    }

    private List<AttributeValue> getAttributes(Page page) {
        try {
            final List<AttributeValue> att = JsonAttributeParserTest.getAtt(page.getAttributes());
            //att.forEach(attributeValue -> attributeValue.setP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private de.cf.model.Page getPages(Page page) {
        return de.cf.model.Page.builder().name(page.getName()).id(page.getId()).build();
    }

    @Test
    public void testReadTypeDefinitions() {
        dbReader.readTypeDefinitions()
                .forEach(System.err::println);
    }

}
